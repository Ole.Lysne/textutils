package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;
import no.uib.ii.inf112.pond.Position;
import no.uib.ii.inf112.pond.impl.Duck;

public class DuckTest {
	Pond pond;
	PondObject obj;
	@BeforeEach
	void setup() {
		pond = new Pond(1280, 720);
		obj = (new Duck(Position.create(0, 0), 25));
	}
	
	@Test
	void addTest() {
		pond.add(obj);
		assertTrue(pond.objs.contains(obj));
	}
	
	@Test
	void moveTest() {
		pond.add(obj);
		double objX = obj.getX();
		pond.step();
		assertNotEquals(objX, obj.getX());
	}
	
	@Test
	void generationTest() {
		for (int i = 0; i < 25; i++) {
			pond.step();
		}
	}
}
