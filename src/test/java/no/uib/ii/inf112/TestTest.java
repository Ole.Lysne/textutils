package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = width-text.length();
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = width-text.length();
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			if (text.length() > width) throw new IllegalArgumentException();
			if (text.length() == width) return text;
			
			String[] words = text.split(" ");
			int extra = width-text.length();
			
			int spacecount = extra / words.length;
			
			for (String w : words) {
				System.out.println(w);
			}
			return null;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
	
	@Test
	void testFlushRight() {
		assertEquals("  test", aligner.flushRight("test", 6));
		assertEquals("     This is a test", aligner.flushRight("This is a test", 19));
	}
	
	@Test
	void testFlushLeft() {
		assertEquals("test   ", aligner.flushLeft("test", 7));
		assertEquals("This is also a test          ", aligner.flushLeft("This is also a test", 29));
	}
	
	@Test
	void testJustify() {
		assertEquals("This   is   a   test", aligner.justify("This is a test", 20));
		assertEquals("This   is  a  test", aligner.justify("This is a test", 18));
	}
}
